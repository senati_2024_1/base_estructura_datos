# Curso: Base y estructura de datos

## Intructor: Ing. Franklin Condori

###  Semana 01

[* Semana 01](/sesion_01/sesion_01.md)

[* Practica calificada 01 ](/practicas_calificadas/practica_01.md)

[* Ejemplos privilegios](/ejemplos/ejemplos_privilegios.md)

[* Ejemplos DCL](/ejemplos/ejemplos_dcl.md)

[* Ejemplos datos optimizados](/ejemplos/ejemplos_datos_optimizados.md)

[* Ejemplos varios](/ejemplos/ejemplos_varios.md)

###  Semana 02

[* Sesion 02](sesion_02/transact_sql/transact_sql.md)

[* Creando la base de datos TiendaDB](/sesion_02/transact_sql/base_datos_tiendadb.md)

[* Creando procedimientos almacenados definidos por el usuario](sesion_02/transact_sql/procedimientos_usuario.md)

[* Manipular consultas multiples](sesion_02/transact_sql/manipular_consultas_multiples.md)

[* Usando sentencias GROUP BY y HAVING](sesion_02/transact_sql/uso_sentencias.md)

[* Ejercicios resuelto](sesion_02/transact_sql/ejercicios_resueltos.md)

[* Ejercicios propuestos](sesion_02/transact_sql/ejercicios_varios.md)

###  Semana 03

[* Base de datos BDVENTAS](sesion_03/bdventa.md)

[* Ejercicios avanzados](sesion_03/consultas_avanzadas.md)

[* Creando base datos en una base de datos no relacional](sesion_03/mongodb.md/)