# Ejemplos de cómo usar COMMIT, SAVEPOINT, ROLLBACK y SET TRANSACTION en SQL Server, MariaDB, SQLite y PostgreSQL utilizando tu base de datos de ejemplo con las tablas Departamento, Empleado, Proyecto y EmpleadoProyecto.

## SQL Server

### COMMIT y ROLLBACK:

```sql:
BEGIN TRANSACTION;

INSERT INTO Departamento (NombreDepartamento, Ubicacion) VALUES ('IT', 'SEATTLE');

-- Si todo va bien
COMMIT;

-- Si algo falla
ROLLBACK;
```

### SAVEPOINT:

```sql:
BEGIN TRANSACTION;

INSERT INTO Departamento (NombreDepartamento, Ubicacion) VALUES ('HR', 'NEW YORK');
SAVE TRANSACTION Savepoint1;

INSERT INTO Empleado (NombreEmpleado, Puesto, IDJefe, FechaContratacion, Salario, Comision, IDDepartamento)
VALUES ('JOHN DOE', 'MANAGER', 7839, '2023-01-01', 3500, NULL, 10);

-- Rollback to Savepoint1 if needed
ROLLBACK TRANSACTION Savepoint1;

-- Commit the transaction
COMMIT;
```

### SET TRANSACTION:

```sql:
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;

BEGIN TRANSACTION;

INSERT INTO Proyecto (NombreProyecto, Ubicacion, IDDepartamento) VALUES ('New Project', 'CHICAGO', 30);

COMMIT;
```

## MariaDB

### COMMIT y ROLLBACK:

```sql:
START TRANSACTION;

INSERT INTO Departamento (NombreDepartamento, Ubicacion) VALUES ('IT', 'SEATTLE');

-- Si todo va bien
COMMIT;

-- Si algo falla
ROLLBACK;
```

### SAVEPOINT:

```sql:
START TRANSACTION;

INSERT INTO Departamento (NombreDepartamento, Ubicacion) VALUES ('HR', 'NEW YORK');
SAVEPOINT Savepoint1;

INSERT INTO Empleado (NombreEmpleado, Puesto, IDJefe, FechaContratacion, Salario, Comision, IDDepartamento)
VALUES ('JOHN DOE', 'MANAGER', 7839, '2023-01-01', 3500, NULL, 10);

-- Rollback to Savepoint1 if needed
ROLLBACK TO Savepoint1;

-- Commit the transaction
COMMIT;
```

### SET TRANSACTION:

```sql:
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;

START TRANSACTION;

INSERT INTO Proyecto (NombreProyecto, Ubicacion, IDDepartamento) VALUES ('New Project', 'CHICAGO', 30);

COMMIT;
```

## SQLite

### COMMIT y ROLLBACK:

```sql:
BEGIN TRANSACTION;

INSERT INTO Departamento (NombreDepartamento, Ubicacion) VALUES ('IT', 'SEATTLE');

-- Si todo va bien
COMMIT;

-- Si algo falla
ROLLBACK;
```

### SAVEPOINT:

```sql:
BEGIN TRANSACTION;

INSERT INTO Departamento (NombreDepartamento, Ubicacion) VALUES ('HR', 'NEW YORK');
SAVEPOINT Savepoint1;

INSERT INTO Empleado (NombreEmpleado, Puesto, IDJefe, FechaContratacion, Salario, Comision, IDDepartamento)
VALUES ('JOHN DOE', 'MANAGER', 7839, '2023-01-01', 3500, NULL, 10);

-- Rollback to Savepoint1 if needed
ROLLBACK TO Savepoint1;

-- Commit the transaction
COMMIT;
```

### SET TRANSACTION:

SQLite no soporta explícitamente el comando SET TRANSACTION con niveles de aislamiento. El nivel de aislamiento en SQLite es SERIALIZABLE por defecto.

## PostgreSQL

#### COMMIT y ROLLBACK:

```sql:
BEGIN;

INSERT INTO Departamento (NombreDepartamento, Ubicacion) VALUES ('IT', 'SEATTLE');

-- Si todo va bien
COMMIT;

-- Si algo falla
ROLLBACK;
```

SAVEPOINT:

```sql:
BEGIN;

INSERT INTO Departamento (NombreDepartamento, Ubicacion) VALUES ('HR', 'NEW YORK');
SAVEPOINT Savepoint1;

INSERT INTO Empleado (NombreEmpleado, Puesto, IDJefe, FechaContratacion, Salario, Comision, IDDepartamento)
VALUES ('JOHN DOE', 'MANAGER', 7839, '2023-01-01', 3500, NULL, 10);

-- Rollback to Savepoint1 if needed
ROLLBACK TO Savepoint1;

-- Commit the transaction
COMMIT;
```

SET TRANSACTION:

```sql:
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;

BEGIN;

INSERT INTO Proyecto (NombreProyecto, Ubicacion, IDDepartamento) VALUES ('New Project', 'CHICAGO', 30);

COMMIT;
```