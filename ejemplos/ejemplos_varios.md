# Ejemplos de cómo usar cursores, alias, parametrización, EXISTS, DISTINCT, TOP, *, verificar si existe un registro y ORDER BY en SQL Server, MariaDB, SQLite y PostgreSQL.

## 1. Cursores

### SQL Server

```sql:
DECLARE @ID INT, @Nombre NVARCHAR(50);
DECLARE EmpleadoCursor CURSOR FOR
SELECT IDEmpleado, NombreEmpleado FROM Empleado;

OPEN EmpleadoCursor;
FETCH NEXT FROM EmpleadoCursor INTO @ID, @Nombre;

WHILE @@FETCH_STATUS = 0
BEGIN
    PRINT 'Empleado ID: ' + CAST(@ID AS NVARCHAR) + ', Nombre: ' + @Nombre;
    FETCH NEXT FROM EmpleadoCursor INTO @ID, @Nombre;
END

CLOSE EmpleadoCursor;
DEALLOCATE EmpleadoCursor;
```

### MariaDB

```sql:
DELIMITER //
CREATE PROCEDURE ListarEmpleados()
BEGIN
    DECLARE done INT DEFAULT 0;
    DECLARE ID INT;
    DECLARE Nombre VARCHAR(50);
    DECLARE EmpleadoCursor CURSOR FOR SELECT IDEmpleado, NombreEmpleado FROM Empleado;
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;

    OPEN EmpleadoCursor;
    REPEAT
        FETCH EmpleadoCursor INTO ID, Nombre;
        IF NOT done THEN
            SELECT ID, Nombre;
        END IF;
    UNTIL done END REPEAT;

    CLOSE EmpleadoCursor;
END//
DELIMITER ;

CALL ListarEmpleados();
```

### SQLite

SQLite no admite cursores directamente en SQL. La gestión del cursor normalmente se realiza en el código de la aplicación.

### PostgreSQL

```sql:
DO $$
DECLARE
    empleado RECORD;
BEGIN
    FOR empleado IN SELECT IDEmpleado, NombreEmpleado FROM Empleado LOOP
        RAISE NOTICE 'Empleado ID: %, Nombre: %', empleado.IDEmpleado, empleado.NombreEmpleado;
    END LOOP;
END $$;
```

## 2. Alias

### SQL Server, MariaDB, SQLite, PostgreSQL

```sql:
SELECT E.NombreEmpleado AS Nombre, D.NombreDepartamento AS Departamento
FROM Empleado E
JOIN Departamento D ON E.IDDepartamento = D.IDDepartamento;
```

## 3. Parametrización

### SQL Server

```sql:
DECLARE @SalarioMin DECIMAL(10, 2) = 2500.00;
SELECT * FROM Empleado WHERE Salario > @SalarioMin;
```

### MariaDB

```sql:
PREPARE stmt FROM 'SELECT * FROM Empleado WHERE Salario > ?';
SET @SalarioMin = 2500.00;
EXECUTE stmt USING @SalarioMin;
DEALLOCATE PREPARE stmt;
```

### SQLite

SQLite no admite variables en SQL directamente. Utilice parámetros en el código de la aplicación.

### PostgreSQL

```sql:
PREPARE stmt (NUMERIC) AS SELECT * FROM Empleado WHERE Salario > $1;
EXECUTE stmt(2500.00);
DEALLOCATE PREPARE stmt;
```

## 4. EXISTS

### SQL Server, MariaDB, SQLite, PostgreSQL

```sql:
SELECT NombreEmpleado
FROM Empleado E
WHERE EXISTS (
    SELECT 1
    FROM EmpleadoProyecto EP
    WHERE EP.IDEmpleado = E.IDEmpleado
);
```

## 5. DISTINCT

### SQL Server, MariaDB, SQLite, PostgreSQL

```sql:
SELECT DISTINCT NombreDepartamento
FROM Departamento;
```

## 6. TOP / LIMIT

### SQL Server

```sql:
SELECT TOP 5 * FROM Empleado ORDER BY Salario DESC;
```
### MariaDB, SQLite, PostgreSQL

```sql:
SELECT * FROM Empleado ORDER BY Salario DESC LIMIT 5;
```

## 7. *

### SQL Server, MariaDB, SQLite, PostgreSQL

```sql:
SELECT * FROM Empleado;
```

## 8. Verificar si existe un registro

### SQL Server

```sql:
IF EXISTS (SELECT 1 FROM Empleado WHERE NombreEmpleado = 'SMITH')
    PRINT 'Empleado existe';
ELSE
    PRINT 'Empleado no existe';
```

### MariaDB

```sql:
IF (SELECT EXISTS (SELECT 1 FROM Empleado WHERE NombreEmpleado = 'SMITH')) THEN
    SELECT 'Empleado existe';
ELSE
    SELECT 'Empleado no existe';
END IF;
```

### SQLite

```sql:
SELECT CASE WHEN EXISTS (SELECT 1 FROM Empleado WHERE NombreEmpleado = 'SMITH')
            THEN 'Empleado existe'
            ELSE 'Empleado no existe'
       END;
```

### PostgreSQL

```sql:
DO $$
BEGIN
    IF EXISTS (SELECT 1 FROM Empleado WHERE NombreEmpleado = 'SMITH') THEN
        RAISE NOTICE 'Empleado existe';
    ELSE
        RAISE NOTICE 'Empleado no existe';
    END IF;
END $$;
```

## 9. ORDER BY

### SQL Server, MariaDB, SQLite, PostgreSQL

```sql:
SELECT * FROM Empleado ORDER BY NombreEmpleado ASC;
```
