# Estructura de Documentos

## Distrito
```json:
{
  "_id": "distrito_id",
  "nombre": "Nombre del Distrito"
}
```

## Vendedor

```json:
{
  "_id": "vendedor_id",
  "nombre": "Nombre",
  "apellido": "Apellido",
  "sueldo": "Sueldo",
  "fecha_inicio": "Fecha de Inicio",
  "tipo": "Tipo de Vendedor",
  "cod_distrito": "distrito_id"
}
```

## Cliente

```json:
{
  "_id": "cliente_id",
  "razon_social": "Razón Social",
  "direccion": "Dirección",
  "telefono": "Teléfono",
  "ruc": "RUC",
  "cod_distrito": "distrito_id",
  "fecha_registro": "Fecha de Registro",
  "tipo": "Tipo de Cliente",
  "contacto": "Contacto"
}
```

# Proveedor

```json:
{
  "_id": "proveedor_id",
  "razon_social": "Razón Social",
  "direccion": "Dirección",
  "telefono": "Teléfono",
  "cod_distrito": "distrito_id",
  "representante": "Representante"
}
```

## Factura

```json:
{
  "_id": "factura_id",
  "fecha": "Fecha",
  "cliente": {
    "id": "cliente_id",
    "razon_social": "Razón Social"
  },
  "fecha_cancelacion": "Fecha de Cancelación",
  "estado": "Estado",
  "vendedor": {
    "id": "vendedor_id",
    "nombre": "Nombre"
  },
  "porcentaje_igv": "Porcentaje de IGV",
  "detalle": [
    {
      "producto_id": "producto_id",
      "cantidad": "Cantidad",
      "precio": "Precio"
    }
  ]
}
```

## OrdenCompra

```json:
{
  "_id": "orden_compra_id",
  "fecha": "Fecha",
  "proveedor": {
    "id": "proveedor_id",
    "razon_social": "Razón Social"
  },
  "fecha_atencion": "Fecha de Atención",
  "estado": "Estado",
  "detalle": [
    {
      "producto_id": "producto_id",
      "cantidad": "Cantidad",
      "precio": "Precio"
    }
  ]
}
```

## Producto

```json:
{
  "_id": "producto_id",
  "descripcion": "Descripción",
  "precio": "Precio",
  "stock_actual": "Stock Actual",
  "stock_minimo": "Stock Mínimo",
  "unidad": "Unidad",
  "linea": "Línea",
  "impuesto": "Impuesto"
}
```

## Abastecimiento

```json:
{
  "proveedor_id": "proveedor_id",
  "producto_id": "producto_id",
  "precio_abastecimiento": "Precio de Abastecimiento"
}
```