# Consultas avanzadas

## Creando Indices
```sql:
use BDVENTAS
go

-- Creando indice en lña tabla FACTURA
CREATE INDEX idx_factura_cod_vendedor ON FACTURA (COD_VEN);

EXEC sp_helpindex 'FACTURA';

select * from FACTURA where COD_VEN = 'V07';

-- Creando indice en lña tabla PRODUCTO
CREATE INDEX idx_producto_descripcion ON PRODUCTO (DES_PRO);

EXEC sp_helpindex 'PRODUCTO';

SELECT * FROM PRODUCTO WHERE DES_PRO='LAPICERO AZUL';

-- Creando indice en lña tabla ORDEN_COMPRA
CREATE INDEX idx_orden_compra_cod_proveedor ON ORDEN_COMPRA (COD_PRV);

EXEC sp_helpindex 'ORDEN_COMPRA';

SELECT * FROM ORDEN_COMPRA WHERE COD_PRV = 'PR07';
```

## Realizando varios tipos de consultas

```sql:
-- 1. Vendedores por cada distrito
SELECT COD_DIS, COUNT(*) AS CANTIDAD_VENDEDORES
FROM VENDEDOR
GROUP BY COD_DIS;

-- 2. Cantidad de clientes agrupados por distrito y tipo de cliente, con un total general.
SELECT COD_DIS, TIP_CLI, COUNT(*) AS CANTIDAD_CLIENTES
FROM CLIENTE
GROUP BY ROLLUP (COD_DIS, TIP_CLI);

-- 3. cubo de todas las combinaciones posibles de distrito, tipo de vendedor y tipo de cliente,
-- contando la cantidad de personas en cada combinación.
SELECT COD_DIS, TIP_VEN, TIP_CLI, COUNT(*) AS CANTIDAD_PERSONAS
FROM (SELECT COD_DIS, TIP_VEN, NULL AS TIP_CLI FROM VENDEDOR
      UNION ALL
      SELECT COD_DIS, NULL AS TIP_VEN, TIP_CLI FROM CLIENTE) AS PERSONAS
GROUP BY CUBE (COD_DIS, TIP_VEN, TIP_CLI);

-- 4. Agrupamiento por conjuntos específicos de columnas: solo por distrito, por distrito y tipo de vendedor,
-- y por distrito y tipo de cliente.
SELECT COD_DIS, TIP_VEN, TIP_CLI, COUNT(*) AS CANTIDAD_PERSONAS
FROM (SELECT COD_DIS, TIP_VEN, NULL AS TIP_CLI FROM VENDEDOR
      UNION ALL
      SELECT COD_DIS, NULL AS TIP_VEN, TIP_CLI FROM CLIENTE) AS PERSONAS
GROUP BY GROUPING SETS ((COD_DIS), (COD_DIS, TIP_VEN), (COD_DIS, TIP_CLI));

-- 5. Calcular el total de productos en la tabla PRODUCTO sin agrupar por ninguna columna específica.
SELECT COUNT(*) AS TOTAL_PRODUCTOS
FROM PRODUCTO;

-- 6. La cláusula ALL se usa para incluir una fila adicional que contiene el recuento total de vendedores,
-- incluso si hay filas con valores NULL en la columna COD_DIS.
SELECT COD_DIS, COUNT(*) AS CANTIDAD_VENDEDORES
FROM VENDEDOR
GROUP BY ALL COD_DIS;

-- 7. Esta consulta realiza un ROLLUP en la columna COD_DIS y agrupa también por TIP_CLI.
SELECT COD_DIS, TIP_CLI, COUNT(*) AS CANTIDAD_CLIENTES
FROM CLIENTE
GROUP BY ROLLUP (COD_DIS), TIP_CLI;
```
