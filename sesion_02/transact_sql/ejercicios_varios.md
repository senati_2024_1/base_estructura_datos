# Ejercicios de Consultas SQL

*Ordenar de menor a mayor y dar formato en soles en todas las consulas que tienen un valor en ventas.*

1. **Total de ventas en el año 2009:**

    - ¿Cuál es el total de ventas realizadas en el año 2009?

2. **Personas sin entradas registradas en la tabla personaDestino:**

    - ¿Cuáles son las personas que no tienen una entrada registrada en la tabla personaDestino?

3. **Promedio del monto total de transacciones de ventas:**

    - ¿Cuál es el promedio del monto total de todas las transacciones de ventas registradas en la base de datos, expresado en moneda local (soles peruanos)?

4. **Documentos de ventas con monto total superior al promedio:**

    - Obtén una lista de todos los documentos de ventas cuyo monto total supere el promedio del monto total de todos los documentos de ventas registrados en la base de datos.

5. **Documentos de ventas pagados con una forma de pago específica:**

    - Listar los documentos de ventas que han sido pagados utilizando una forma de pago específica desde la tabla documentoPago.

6. **Detalles de documentos de ventas canjeados:**

    - ¿Cómo se distribuye el saldo total entre los diferentes almacenes, considerando la información de los saldos iniciales de inventario en la base de datos?

7. **Saldo total distribuido por almacén:**

    - Obtén una lista de todos los documentos de ventas cuyo monto total supere el promedio del monto total de todos los documentos de ventas registrados en la base de datos.

8. **Detalles de documentos de ventas por vendedor:**

    - ¿Cuáles son los detalles de los documentos de ventas asociados al vendedor con identificación número 3 en la base de datos, considerando la información detallada de cada documento en relación con sus elementos de venta?

9. **Total de ventas por año y vendedor:**

    - ¿Cuál es el total de ventas por año y vendedor en la base de datos de ventas, considerando solo aquellos vendedores cuya suma total de ventas en un año específico sea superior a 100,000 unidades monetarias?

10. **Desglose mensual de ventas por vendedor:**

    - ¿Cuál es el desglose mensual de las ventas por vendedor en cada año, considerando la suma total de ventas para cada mes y año específico?

11. **Clientes que compraron más de 10 veces en un año:**

    - ¿Cuántos clientes compraron más de 10 veces en un año?

12. **Total acumulado de descuentos por vendedor:**

    - ¿Cuál es el total acumulado de descuentos aplicados por cada vendedor en la base de datos de ventas, considerando la suma de los descuentos descto01, descto02 y descto03, y mostrando solo aquellos vendedores cuyo total de descuentos acumulados supere los 5000?

13. **Total anual de ventas por persona:**

    - ¿Cuál es el total anual de ventas realizadas por cada persona en la base de datos de ventas, considerando únicamente los movimientos de tipo venta (tipoMovimiento = 1), y mostrando solo aquellas personas cuyas ventas anuales superen los 10000?

14. **Recuento total de productos vendidos por vendedor:**

    - ¿Cuál es el recuento total de productos vendidos por cada vendedor en la base de datos de ventas?

15. **Ventas mensuales desglosadas por tipo de pago:**

    - ¿Cuánto se vendió cada mes del año 2009, desglosado por tipo de pago?

16. **Total de ventas en el año 2007:**

    - ¿Cuál es el total de ventas realizadas en el año 2007?

17. **Personas sin entradas registradas en la tabla personaDestino en el año 2008:**

    - ¿Cuáles son las personas que no tienen una entrada registrada en la tabla personaDestino en el año 2008?

18. **Promedio del monto total de transacciones de ventas en el año 2009:**

    - ¿Cuál es el promedio del monto total de todas las transacciones de ventas registradas en la base de datos en el año 2009, expresado en moneda local (soles peruanos)?

19. **Documentos de ventas con monto total superior al promedio en el año 2005:**

    - Obtén una lista de todos los documentos de ventas cuyo monto total supere el promedio del monto total de todos los documentos de ventas registrados en la base de datos en el año 2005.

20. **Documentos de ventas pagados con una forma de pago específica en el año 2006:**

    - Listar los documentos de ventas que han sido pagados utilizando una forma de pago específica desde la tabla documentoPago en el año 2006.

21. **Detalles de documentos de ventas canjeados en el año 2007:**

    - ¿Cómo se distribuye el saldo total entre los diferentes almacenes, considerando la información de los saldos iniciales de inventario en la base de datos en el año 2007?

22. **Saldo total distribuido por almacén en el año 2008:**

    - Obtén una lista de todos los documentos de ventas cuyo monto total supere el promedio del monto total de todos los documentos de ventas registrados en la base de datos en el año 2008.

23. **Detalles de documentos de ventas por vendedor en el año 2009:**

    - ¿Cuáles son los detalles de los documentos de ventas asociados al vendedor con identificación número 3 en la base de datos en el año 2009, considerando la información detallada de cada documento en relación con sus elementos de venta?

24. **Total de ventas por año y vendedor en el año 2008:**

    - ¿Cuál es el total de ventas por año y vendedor en la base de datos de ventas, considerando solo aquellos vendedores cuya suma total de ventas en el año 2008 sea superior a 100,000 unidades monetarias?

25. **Desglose mensual de ventas por vendedor en el año 2009:**

    - ¿Cuál es el desglose mensual de las ventas por vendedor en cada año, considerando la suma total de ventas para cada mes y año específico en el año 2009?

26. **Clientes que compraron más de 10 veces en un año en el año 2005:**

    - ¿Cuántos clientes compraron más de 10 veces en un año en el año 2005?

27. **Total acumulado de descuentos por vendedor en el año 2006:**

    - ¿Cuál es el total acumulado de descuentos aplicados por cada vendedor en la base de datos de ventas, considerando la suma de los descuentos descto01, descto02 y descto03, y mostrando solo aquellos vendedores cuyo total de descuentos acumulados supere los 5000 en el año 2005?

28. **Total anual de ventas por persona en el año 2007:**

    - ¿Cuál es el total anual de ventas realizadas por cada persona en la base de datos de ventas, considerando únicamente los movimientos de tipo venta (tipoMovimiento = 1), y mostrando solo aquellas personas cuyas ventas anuales superen los 10000 en el año 2007?

29. **Recuento total de productos vendidos por vendedor en el año 2008:**

    - ¿Cuál es el recuento total de productos vendidos por cada vendedor en la base de datos de ventas en el año 2008?

30. **Ventas mensuales desglosadas por tipo de pago en el año 2009:**

    - ¿Cuánto se vendió cada mes del año 2009, desglosado por tipo de pago?


