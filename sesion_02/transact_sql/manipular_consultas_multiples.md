# Manipulacion de datos con consultas múltiples

A continuación se presentan ejemplos de uso de UNION, UNION ALL, INTERSECT, y EXCEPT.

* UNION: Combina los resultados de dos consultas y elimina los duplicados. Es útil cuando necesitas combinar filas de dos tablas o resultados de dos consultas diferentes y quieres evitar duplicados.

* UNION ALL: Similar a UNION, pero no elimina duplicados. Es más eficiente que UNION porque no tiene que eliminar duplicados, por lo que se usa cuando sabes que no habrá duplicados o cuando no te importan.

* INTERSECT: Devuelve los registros que son comunes a ambas consultas. Es útil para encontrar registros que aparecen en ambos conjuntos de resultados.

* EXCEPT: Devuelve los registros que están en el primer conjunto de resultados pero no en el segundo. Se usa para encontrar diferencias entre dos conjuntos de datos.

**Para MariaDB, donde INTERSECT y EXCEPT no están soportados directamente, se usan JOINs para simular su comportamiento.**

## SQL Server

```sql:
-- UNION: Combina los resultados de dos consultas y elimina duplicados.
SELECT nombre FROM articulo
UNION
SELECT nombre FROM categoria;

-- UNION ALL: Combina los resultados de dos consultas y no elimina duplicados.
SELECT nombre FROM articulo
UNION ALL
SELECT nombre FROM categoria;

-- INTERSECT: Devuelve los registros que son comunes en ambas consultas.
SELECT nombre FROM articulo
INTERSECT
SELECT nombre FROM categoria;

-- EXCEPT: Devuelve los registros que están en la primera consulta pero no en la segunda.
SELECT nombre FROM articulo
EXCEPT
SELECT nombre FROM categoria;
```

## MariaDB

```sql:
-- MariaDB no soporta INTERSECT y EXCEPT directamente, pero se pueden simular con JOINs

-- UNION: Combina los resultados de dos consultas y elimina duplicados.
SELECT nombre FROM articulo
UNION
SELECT nombre FROM categoria;

-- UNION ALL: Combina los resultados de dos consultas y no elimina duplicados.
SELECT nombre FROM articulo
UNION ALL
SELECT nombre FROM categoria;

-- Simulando INTERSECT: Devuelve los registros que son comunes en ambas consultas.
SELECT nombre FROM articulo
INNER JOIN (SELECT nombre FROM categoria) AS c
ON articulo.nombre = c.nombre;

-- Simulando EXCEPT: Devuelve los registros que están en la primera consulta pero no en la segunda.
SELECT nombre FROM articulo
LEFT JOIN (SELECT nombre FROM categoria) AS c
ON articulo.nombre = c.nombre
WHERE c.nombre IS NULL;
```

## SQLite

```sql:
-- UNION: Combina los resultados de dos consultas y elimina duplicados.
SELECT nombre FROM articulo
UNION
SELECT nombre FROM categoria;

-- UNION ALL: Combina los resultados de dos consultas y no elimina duplicados.
SELECT nombre FROM articulo
UNION ALL
SELECT nombre FROM categoria;

-- INTERSECT: Devuelve los registros que son comunes en ambas consultas.
SELECT nombre FROM articulo
INTERSECT
SELECT nombre FROM categoria;

-- EXCEPT: Devuelve los registros que están en la primera consulta pero no en la segunda.
SELECT nombre FROM articulo
EXCEPT
SELECT nombre FROM categoria;
```

## PostgreSQL

```sql:
-- UNION: Combina los resultados de dos consultas y elimina duplicados.
SELECT nombre FROM articulo
UNION
SELECT nombre FROM categoria;

-- UNION ALL: Combina los resultados de dos consultas y no elimina duplicados.
SELECT nombre FROM articulo
UNION ALL
SELECT nombre FROM categoria;

-- INTERSECT: Devuelve los registros que son comunes en ambas consultas.
SELECT nombre FROM articulo
INTERSECT
SELECT nombre FROM categoria;

-- EXCEPT: Devuelve los registros que están en la primera consulta pero no en la segunda.
SELECT nombre FROM articulo
EXCEPT
SELECT nombre FROM categoria;
```

## Ejercicios propuestos

### UNION

* Ejercicio 1:

    Seleccionar los tipos de comprobante de los productos vendidos en el mes de enero del 2024 y los nombres de los productos en stock actualmente, combinando ambas listas sin duplicados.

* Ejercicio 2:

    Obtener los nombres de los clientes que realizaron una compra en la tienda online y los nombres de los clientes que visitaron la tienda física durante el mes de enero del 2024, sin repetir nombres.

### UNION ALL

* Ejercicio 1:

    Seleccionar los nombres de los productos vendidos en el mes de enero del 2024 y los nombres de los productos en stock actualmente, combinando ambas listas incluyendo duplicados.

* Ejercicio 2:

    Obtener los nombres de los clientes que realizaron una compra en la tienda online y los nombres de los clientes que visitaron la tienda física durante el mes de enero del 2024, incluyendo duplicados.

### INTERSECT

* Ejercicio 1:

    Encontrar los nombres de los productos que están tanto en la lista de productos más vendidos como en la lista de productos con mejor reseña.

* Ejercicio 2:

    Identificar los clientes que realizaron una compra en el mes de enero del 2024 y que también han visitado la tienda en este mes.

### EXCEPT

* Ejercicio 1:

    Listar los nombres de los productos que están en el inventario pero que no han sido vendidos en el mes de enero del 2024.

* Ejercicio 2:

    Encontrar los clientes que han visitado la tienda física pero que no han realizado ninguna compra en el mes de enero del 2024.