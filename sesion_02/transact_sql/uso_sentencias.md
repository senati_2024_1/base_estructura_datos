# Usar sentencias GROUP BY y HAVING

Se muestra un ejemplo donde, se calcula el total de ventas para cada cliente usando GROUP BY y luego filtran esos resultados utilizando la cláusula HAVING para mostrar solo aquellos clientes cuyo total de ventas sea mayor a $1000.

## SQL Server

```sql:
-- Calcular el total de ventas por cada cliente y filtrar aquellos con total de ventas mayor a $1000
SELECT idcliente, SUM(total) AS total_ventas
FROM venta
GROUP BY idcliente
HAVING SUM(total) >= 1000;
```

## MariaDB

```sql:
-- Calcular el total de ventas por cada cliente y filtrar aquellos con total de ventas mayor a $1000
SELECT idcliente, SUM(total) AS total_ventas
FROM venta
GROUP BY idcliente
HAVING SUM(total) >= 1000;
```

## SQLite

```sql:
-- Calcular el total de ventas por cada cliente y filtrar aquellos con total de ventas mayor a $1000
SELECT idcliente, SUM(total) AS total_ventas
FROM venta
GROUP BY idcliente
HAVING SUM(total) >= 1000;
```

## PostgreSQL

```sql:
-- Calcular el total de ventas por cada cliente y filtrar aquellos con total de ventas mayor a $1000
SELECT idcliente, SUM(total) AS total_ventas
FROM venta
GROUP BY idcliente
HAVING SUM(total) >= 1000;
```

## Mas ejercicios

1. Calcular el total de ventas por cada cliente y filtrar aquellos con total de ventas mayor a $1000.

### Script de la consulta

```sql:
SELECT idcliente, SUM(total) AS total_ventas
FROM venta
GROUP BY idcliente
HAVING SUM(total) >= 1000;
```

2. Calcular el número de ventas pendiente por cada vendedor y mostrar solo aquellos vendedores que tienen más o igual a 1 ventas completadas.

```sql:
SELECT idusuario, COUNT(*) AS ventas_completadas
FROM venta
WHERE estado = 'Pendiente'
GROUP BY idusuario
HAVING COUNT(*) >= 1;
```

3. Calcular el promedio de precios de venta por categoría de productos y mostrar solo aquellas categorías cuyo promedio de precio de venta sea mayor a $200.

```sql:
SELECT c.nombre AS categoria, AVG(a.precio_venta) AS precio_promedio
FROM articulo a
INNER JOIN categoria c ON a.idcategoria = c.idcategoria
GROUP BY c.nombre
HAVING AVG(a.precio_venta) > 200;
```

4. Calcular la cantidad de ventas por día de la semana y mostrar solo aquellos días donde la cantidad de ventas sea mayor a 10.

```sql:
SELECT DATENAME(WEEKDAY, fecha) AS dia_semana, COUNT(*) AS cantidad_ventas
FROM venta
GROUP BY DATENAME(WEEKDAY, fecha)
HAVING COUNT(*) > 10;
```

5. Calcular el total de ingresos por proveedor y mostrar solo aquellos proveedores cuyo total de ingresos sea mayor a $500.

```sql:
SELECT p.nombre AS proveedor, SUM(i.total) AS total_ingresos
FROM ingreso i
INNER JOIN persona p ON i.idproveedor = p.idpersona
GROUP BY p.nombre
HAVING SUM(i.total) > 500;
```

6. Calcular la cantidad de ventas por año y mes, y mostrar solo aquellos años donde la cantidad de ventas sea mayor a 50

```sql:
SELECT
	YEAR(convert(DATE, fecha)) AS anio,
	MONTH(convert(DATE, fecha)) AS mes,
	COUNT(*) AS cantidad_ventas
FROM venta
GROUP BY YEAR(convert(DATE, fecha)), MONTH(convert(DATE,fecha))
HAVING COUNT(*) > 50;
```

7. Calcular el total de ventas por tipo de comprobante y mostrar solo aquellos tipos de comprobante donde el total de ventas sea mayor a $100.

```sql:
SELECT tipo_comprobante, SUM(total) AS total_ventas
FROM venta
GROUP BY tipo_comprobante
HAVING SUM(total) > 100;
```

8. Calcular el promedio de ventas mensuales por vendedor y mostrar solo aquellos vendedores cuyo promedio mensual de ventas sea mayor a $300.

```sql:
SELECT idusuario, AVG(total) AS promedio_ventas_mensuales
FROM venta
GROUP BY idusuario
HAVING AVG(total) > 300;
```

9. Calcular el número de ventas por día de la semana y mostrar solo aquellos días donde el número de ventas sea mayor a 15

```sql:
SELECT DAYNAME(fecha) AS dia_semana, COUNT(*) AS num_ventas
FROM venta
GROUP BY DAYNAME(fecha)
HAVING COUNT(*) > 15;
```

10. Calcular el total de ingresos por mes y año, y mostrar solo aquellos meses y años donde el total de ingresos sea mayor a $100

```sql:
SELECT STRFTIME('%Y-%m', fecha) AS anio_mes, SUM(total) AS total_ingresos
FROM ingreso
GROUP BY STRFTIME('%Y-%m', fecha)
HAVING SUM(total) > 100;
```

11. Calcular el total de ventas por cliente y mostrar solo aquellos clientes cuyo total de ventas sea mayor a $50 y que tengan al menos 1 ventas

```sql:
SELECT idcliente, COUNT(*) AS total_ventas, SUM(total) AS total_ventas_dinero
FROM venta
GROUP BY idcliente
HAVING SUM(total) > 50 AND COUNT(*) >= 1;
```

## Ejercicios propuestos

* Ejercicio 1

    Calcular el total de ventas por cada tipo de comprobante de los clientes y mostrar solo aquellos cuyo total de ventas sea mayor a $50.

* Ejercicio 2

    Calcular el número de productos vendidos por cada vendedor y mostrar solo aquellos vendedores que han vendido más de 50 en total.

* Ejercicio 3

    Calcular el promedio de ingresos y mostrar solo aquellas promedio de ingresos mensuales sea mayor a $10.

* Ejercicio 4

    Calcular la cantidad de ventas completadas por cada tipo de comprobante (Factura, Boleta) y mostrar solo aquellos tipos de comprobante que tienen más de 1 ventas completadas.

* Ejercicio 5

    Calcular el promedio de venta por cada articulo y mostrar solo aquellas cuyo promedio de precio de venta sea mayor a $30.

* Ejercicio 6

    Calcular el total de ventas por cada mes y mostrar solo aquellos donde el total de ventas sea mayor a $200.

* Ejercicio 7

    Calcular el número de ingresos por cada categoría de productos y mostrar solo aquellas categorías que tienen más o igual a 1 ingresos registrado.

* Ejercicio 8

    Calcular el promedio de ventas mensuales por cada vendedor y mostrar solo aquellos vendedores cuyo promedio mensual de ventas sea mayor a $70.
