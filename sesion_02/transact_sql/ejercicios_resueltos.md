# Ejercicios de Consultas SQL

## 1. ¿Cuál es el total de ventas realizadas en el año 2009?

```sql
DECLARE @fecha_inicio datetime = '2024-01-01';
DECLARE @fecha_fin datetime = '2024-12-31';

SELECT SUM(total) AS Total_Ventas
FROM [ve].[documento]
WHERE fechaMovimiento BETWEEN @fecha_inicio AND @fecha_fin;
```

## 2. ¿Cuáles son las personas que no tienen una entrada registrada en la tabla personaDestino?

```sql
SELECT p.*
FROM [ma].[persona] p
LEFT JOIN [ma].[personaDestino] pd ON p.persona = pd.persona
WHERE pd.persona IS NULL;
```

## 3. ¿Cuál es el promedio del monto total de todas las transacciones de ventas registradas en la base de datos, expresado en moneda local (soles peruanos)?

```sql
SELECT FORMAT(AVG(total), 'C', 'es-PE') AS [Promedio del Monto Total en Soles]
FROM [ve].[documento];
```

## 4. Obtén una lista de todos los documentos de ventas cuyo monto total supere el promedio del monto total de todos los documentos de ventas registrados en la base de datos.

```sql
SELECT *
FROM [ve].[documento]
WHERE total > (SELECT AVG(total) FROM [ve].[documento]);
```

## 5. Listar los documentos de ventas que han sido pagados utilizando una forma de pago específica desde [ve].[documentoPago]

```sql
SELECT d.*
FROM [ve].[documentoPago] dp
INNER JOIN [ve].[documento] d ON dp.documento = d.documento
INNER JOIN [pa].[pago] p ON dp.pago = p.pago
WHERE p.formaPago = 1; -- 1 es pago al Contado
```

## 6. ¿Cuál es la consulta que devuelve los detalles de los documentos de ventas que han sido canjeados, considerando que un documento puede ser canjeado por uno o más documentos, y estos detalles deben incluir información de los documentos originales y los documentos canjeados?

```sql
SELECT d.*, dc.*
FROM [ve].[documento] d
INNER JOIN [ve].[documentosCanjeados] dc ON d.documento = dc.documento01 OR d.documento = dc.documento02;
```

## 7. "¿Cómo se distribuye el saldo total entre los diferentes almacenes, considerando la información de los saldos iniciales de inventario en la base de datos?"

```sql
SELECT almacen, SUM(constoSoles) AS Saldo_Total
FROM [ma].[saldosIniciales]
GROUP BY almacen;
```

## 8. ¿Cuáles son los detalles de los documentos de ventas asociados al vendedor con identificación número 3 en la base de datos, considerando la información detallada de cada documento en relación con sus elementos de venta?

```sql
SELECT d.*
FROM [ve].[documento] d
INNER JOIN [ve].[documentoDetalle] dd ON d.documento = dd.documento
WHERE d.vendedor = 3;
```

## 9. ¿Cuál es el total de ventas por año y vendedor en la base de datos de ventas, considerando solo aquellos vendedores cuya suma total de ventas en un año específico sea superior a 100,000 unidades monetarias?

```sql
SELECT vendedor, YEAR(fechaMovimiento) AS Anio, SUM(total) AS Total_Ventas
FROM ve.documento
GROUP BY vendedor, YEAR(fechaMovimiento)
HAVING SUM(total) > 100000;
```

## 10. ¿Cuál es el desglose mensual de las ventas por vendedor en cada año, considerando la suma total de ventas para cada mes y año específico?

```sql
SELECT vendedor, MONTH(fechaMovimiento) AS Anio, SUM(total) AS Total_Ventas
FROM ve.documento
GROUP BY vendedor, MONTH(fechaMovimiento)
HAVING SUM(total) > 100000;
```

## 11. ¿Cuántos clientes compraron más de 10 veces en un año?

```sql
SELECT persona, YEAR(fechaMovimiento) AS Año, COUNT(*) AS Compras
FROM ve.documento
WHERE tipoMovimiento = 1
GROUP BY persona, YEAR(fechaMovimiento)
HAVING COUNT(*) > 10;
```

## 12. ¿Cuál es el total acumulado de descuentos aplicados por cada vendedor en la base de datos de ventas, considerando la suma de los descuentos descto01, descto02 y descto03, y mostrando solo aquellos vendedores cuyo total de descuentos acumulados supere los 5000?

```sql
SELECT vendedor, SUM(descto01 + descto02 + descto03) AS Descuentos_Acumulados
FROM ve.documento
GROUP BY vendedor
HAVING SUM(descto01 + descto02 + descto03) > 5000;
```

## 13. ¿Cuál es el total anual de ventas realizadas por cada persona en la base de datos de ventas, considerando únicamente los movimientos de tipo venta (tipoMovimiento = 1), y mostrando solo aquellas personas cuyas ventas anuales superen los 10000?

```sql
SELECT persona, YEAR(fechaMovimiento) AS Año, SUM(total) AS Total_Anual
FROM ve.documento
WHERE tipoMovimiento = 1
GROUP BY persona, YEAR(fechaMovimiento)
HAVING SUM(total) > 10000;
```

## 14. ¿Cuál es el recuento total de productos vendidos por cada vendedor en la base de datos de ventas?

```sql
SELECT 
    d.vendedor, 
    COUNT(dd.documentoDetalle) AS Total_Productos_Vendidos
FROM 
    ve.documentoDetalle dd
JOIN 
    ve.documento d ON dd.documento = d.documento
GROUP BY 
    d.vendedor;
```

## 15. ¿Cuánto se vendió cada mes del año 2009, desglosado por tipo de pago?

```sql
SELECT 
    MONTH(d.fechaMovimiento) AS Mes,
    p.formaPago,
    SUM(d.total) AS Total_Ventas
FROM 
    ve.documento d
JOIN 
    pa.pago p ON d.vendedor = p.vendedor
WHERE 
    YEAR(d.fechaMovimiento) = 2009
GROUP BY 
    MONTH(d.fechaMovimiento),
    p.formaPago;
```

