# Procedimientos almacenados definidos por el usuario

Ejemplos simples de procedimientos almacenados definidos por el usuario para SQL Server, MariaDB, SQLite y PostgreSQL.

**Estos procedimientos realizan operaciones básicas en la tabla de "articulo", como la inserción, actualización y eliminación de registros.**

## Script

### SQL Server

```sql:
-- Procedimiento almacenado para insertar un nuevo artículo en SQL Server
CREATE PROCEDURE InsertarArticulo
    @idcategoria INT,
    @codigo VARCHAR(50),
    @nombre VARCHAR(100),
    @precio_venta DECIMAL(11, 2),
    @stock INT,
    @descripcion VARCHAR(255),
    @imagen VARCHAR(100)
AS
BEGIN
    INSERT INTO articulo (idcategoria, codigo, nombre, precio_venta, stock, descripcion, imagen, estado)
    VALUES (@idcategoria, @codigo, @nombre, @precio_venta, @stock, @descripcion, @imagen, 1);
END;
GO

-- Procedimiento almacenado para actualizar un artículo en SQL Server
CREATE PROCEDURE ActualizarArticulo
    @idarticulo INT,
    @nombre VARCHAR(100),
    @precio_venta DECIMAL(11, 2),
    @stock INT
AS
BEGIN
    UPDATE articulo
    SET nombre = @nombre, precio_venta = @precio_venta, stock = @stock
    WHERE idarticulo = @idarticulo;
END;
GO

-- Procedimiento almacenado para eliminar un artículo en SQL Server
CREATE PROCEDURE EliminarArticulo
    @idarticulo INT
AS
BEGIN
    DELETE FROM articulo WHERE idarticulo = @idarticulo;
END;
GO
```

### MariaDB

```sql:
-- Procedimiento almacenado para insertar un nuevo artículo en MariaDB
DELIMITER //
CREATE PROCEDURE InsertarArticulo(
    IN p_idcategoria INT,
    IN p_codigo VARCHAR(50),
    IN p_nombre VARCHAR(100),
    IN p_precio_venta DECIMAL(11, 2),
    IN p_stock INT,
    IN p_descripcion VARCHAR(255),
    IN p_imagen VARCHAR(100)
)
BEGIN
    INSERT INTO articulo (idcategoria, codigo, nombre, precio_venta, stock, descripcion, imagen, estado)
    VALUES (p_idcategoria, p_codigo, p_nombre, p_precio_venta, p_stock, p_descripcion, p_imagen, 1);
END //
DELIMITER ;
```

```sql:
-- Procedimiento almacenado para actualizar un artículo en MariaDB
DELIMITER //
CREATE PROCEDURE ActualizarArticulo(
    IN p_idarticulo INT,
    IN p_nombre VARCHAR(100),
    IN p_precio_venta DECIMAL(11, 2),
    IN p_stock INT
)
BEGIN
    UPDATE articulo
    SET nombre = p_nombre, precio_venta = p_precio_venta, stock = p_stock
    WHERE idarticulo = p_idarticulo;
END //
DELIMITER ;
```

```sql:
-- Procedimiento almacenado para eliminar un artículo en MariaDB
DELIMITER //
CREATE PROCEDURE EliminarArticulo(
    IN p_idarticulo INT
)
BEGIN
    DELETE FROM articulo WHERE idarticulo = p_idarticulo;
END //
DELIMITER ;
```

### SQLite

```sql:
-- Procedimiento almacenado para insertar un nuevo artículo en SQLite
CREATE PROCEDURE InsertarArticulo (
    p_idcategoria INTEGER,
    p_codigo TEXT,
    p_nombre TEXT,
    p_precio_venta REAL,
    p_stock INTEGER,
    p_descripcion TEXT,
    p_imagen TEXT
)
AS
BEGIN
    INSERT INTO articulo (idcategoria, codigo, nombre, precio_venta, stock, descripcion, imagen, estado)
    VALUES (p_idcategoria, p_codigo, p_nombre, p_precio_venta, p_stock, p_descripcion, p_imagen, 1);
END;
-- Procedimiento almacenado para eliminar un artículo en SQLite
CREATE PROCEDURE EliminarArticulo (
    p_idarticulo INTEGER
)
AS
BEGIN
    DELETE FROM articulo WHERE idarticulo = p_idarticulo;
END;
```

```sql:
-- Procedimiento almacenado para actualizar un artículo en SQLite
CREATE PROCEDURE ActualizarArticulo (
    p_idarticulo INTEGER,
    p_nombre TEXT,
    p_precio_venta REAL,
    p_stock INTEGER
)
AS
BEGIN
    UPDATE articulo
    SET nombre = p_nombre, precio_venta = p_precio_venta, stock = p_stock
    WHERE idarticulo = p_idarticulo;
END;
```

```sql:
-- Procedimiento almacenado para eliminar un artículo en SQLite
CREATE PROCEDURE EliminarArticulo (
    p_idarticulo INTEGER
)
AS
BEGIN
    DELETE FROM articulo WHERE idarticulo = p_idarticulo;
END;
```

### PostgreSQL

```sql:
-- Procedimiento almacenado para insertar un nuevo artículo en PostgreSQL
CREATE OR REPLACE PROCEDURE InsertarArticulo (
    p_idcategoria INT,
    p_codigo VARCHAR(50),
    p_nombre VARCHAR(100),
    p_precio_venta DECIMAL(11, 2),
    p_stock INT,
    p_descripcion VARCHAR(255),
    p_imagen VARCHAR(100)
)
AS $$
BEGIN
    INSERT INTO articulo (idcategoria, codigo, nombre, precio_venta, stock, descripcion, imagen, estado)
    VALUES (p_idcategoria, p_codigo, p_nombre, p_precio_venta, p_stock, p_descripcion, p_imagen, 1);
END;
$$ LANGUAGE plpgsql;
```

```sql:
-- Procedimiento almacenado para actualizar un artículo en PostgreSQL
CREATE OR REPLACE PROCEDURE ActualizarArticulo (
    p_idarticulo INT,
    p_nombre VARCHAR(100),
    p_precio_venta DECIMAL(11, 2),
    p_stock INT
)
AS $$
BEGIN
    UPDATE articulo
    SET nombre = p_nombre, precio_venta = p_precio_venta, stock = p_stock
    WHERE idarticulo = p_idarticulo;
END;
$$ LANGUAGE plpgsql;
```

```sql:
-- Procedimiento almacenado para eliminar un artículo en PostgreSQL
CREATE OR REPLACE PROCEDURE EliminarArticulo (
    p_idarticulo INT
)
AS $$
BEGIN
    DELETE FROM articulo WHERE idarticulo = p_idarticulo;
END;
$$ LANGUAGE plpgsql;
```

## Ejecutando los procedimientos almacenados

### SQL Server

```sql:
-- Ejemplo de llamada al procedimiento InsertarArticulo en SQL Server
EXEC InsertarArticulo @idcategoria = 1, @codigo = 'ART001', @nombre = 'Laptop Acer', @precio_venta = 699.99, @stock = 10, @descripcion = 'Laptop con procesador Intel Core i5 y 8GB de RAM', @imagen = 'laptop_acer.jpg';
```

```sql:
-- Ejemplo de llamada al procedimiento ActualizarArticulo en SQL Server
EXEC ActualizarArticulo @idarticulo = 1, @nombre = 'Laptop Acer Predator', @precio_venta = 799.99, @stock = 12;
```

```sql:
-- Ejemplo de llamada al procedimiento EliminarArticulo en SQL Server
EXEC EliminarArticulo @idarticulo = 1;
```

### MariaDB

```sql:
-- Ejemplo de llamada al procedimiento InsertarArticulo en MariaDB
CALL InsertarArticulo(1, 'ART001', 'Laptop Acer', 699.99, 10, 'Laptop con procesador Intel Core i5 y 8GB de RAM', 'laptop_acer.jpg');
```

```sql:
-- Ejemplo de llamada al procedimiento ActualizarArticulo en MariaDB
CALL ActualizarArticulo(1, 'Laptop Acer Predator', 799.99, 12);
```

```sql:
-- Ejemplo de llamada al procedimiento EliminarArticulo en MariaDB
CALL EliminarArticulo(1);
```

### SQLite

SQLite no admite procedimientos almacenados. Tenemos que definir funciones y usarlos directamente en nuestras aplicaciones.

### PostgreSQL

```sql:
-- Ejemplo de llamada al procedimiento InsertarArticulo en PostgreSQL
CALL InsertarArticulo(1, 'ART001', 'Laptop Acer', 699.99, 10, 'Laptop con procesador Intel Core i5 y 8GB de RAM', 'laptop_acer.jpg');
```

```sql:
-- Ejemplo de llamada al procedimiento ActualizarArticulo en PostgreSQL
CALL ActualizarArticulo(1, 'Laptop Acer Predator', 799.99, 12);
```

```sql:
-- Ejemplo de llamada al procedimiento EliminarArticulo en PostgreSQL
CALL EliminarArticulo(1);
```

## Tarea: Crear los procedimietos almacenados para crear, actualizar y eliminar para todas las tablas restantes
