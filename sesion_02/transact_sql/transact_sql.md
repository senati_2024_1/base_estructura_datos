# Descripción de T-SQL para la programación

Transact-SQL (T-SQL) es una extensión propietaria del estándar abierto Lenguaje de consulta estructurado (SQL). Admite variables declaradas, procesamiento de cadenas y datos, control de errores y excepciones y control de transacciones. Aunque SQL es un lenguaje de programación, T-SQL añade compatibilidad con la programación de procedimientos y el uso de variables locales.

Un programa T-SQL normalmente comenzará con una instrucción BEGIN y finalizará con una instrucción END, con las instrucciones que quiera ejecutar entre ambas.

A medida que pase de ejecutar objetos de código a crearlos, deberá comprender cómo interactúan varias instrucciones con el servidor durante la ejecución. A medida que desarrolle programas, deberá almacenar temporalmente los valores. Por ejemplo, es posible que tenga que almacenar temporalmente valores que se usarán como parámetros en procedimientos almacenados.

Por último, es posible que quiera crear alias o punteros a objetos para que pueda hacer referencia a ellos con un nombre diferente o desde una ubicación distinta de donde se definen.

Estas son algunas de las estructuras de programación de T-SQL compatibles:

* IF...ELSE: instrucción condicional que permite decidir qué aspectos del código se ejecutarán.
* WHILE: instrucción de bucle que es ideal para ejecutar iteraciones de instrucciones T-SQL.
* DECLARE: lo usará para definir variables.
* SET: una de las maneras en las que asignará valores a las variables.
* BATCHES: serie de instrucciones T-SQL que se ejecutan como una unidad.

¿Qué es transact sql y para que sirve?

T-SQL, es una extensión del lenguaje SQL y se utiliza principalmente en Microsoft SQL Server y Sybase ASE. Además de las características estándar de SQL, T-SQL incorpora funcionalidades adicionales específicas para la programación, como el control de flujo, el manejo de errores y los procedimientos almacenados. Estas extensiones permiten a los desarrolladores escribir consultas más complejas y ejecutar operaciones avanzadas en bases de datos.

Entre las principales características de T-SQL se incluyen:

* Procedimientos almacenados: Facilitan la agrupación y reutilización del código SQL, incrementando la eficiencia y la seguridad.

* Funciones definidas por el usuario: Permiten la creación de funciones personalizadas para su empleo en consultas.

* Variables locales: Pueden declararse y usarse dentro de scripts y procedimientos.

* Control de flujo: Instrucciones como IF...ELSE, WHILE y BEGIN...END permiten controlar la lógica del script.

* Manejo de errores: Instrucciones como TRY...CATCH permiten gestionar y responder a errores de manera estructurada.

* Cursores: Facilitan la iteración detallada sobre las filas de un conjunto de resultados.

## Tipos de procedimientos almacenados 

Los procedimientos almacenados son bloques de código SQL que se guardan en la base de datos y pueden ejecutarse en varias ocasiones sin necesidad de reescribir el código. En T-SQL, los tipos de procedimientos almacenados pueden clasificarse de la siguiente manera:

### 1. Procedimientos almacenados del sistema:

Son procedimientos predefinidos proporcionados por el sistema de gestión de bases de datos (DBMS). Se utilizan para realizar tareas administrativas y de mantenimiento en la base de datos.

### 2. Procedimientos almacenados definidos por el usuario:

Son procedimientos creados por los usuarios para realizar tareas específicas que no están cubiertas por los procedimientos del sistema. Pueden aceptar parámetros de entrada y devolver resultados.

### 3. Procedimientos almacenados temporales:

Son procedimientos que se crean de forma temporal y se eliminan automáticamente cuando la sesión que los creó finaliza o cuando se reinicia el servidor.

### 4. Procedimientos almacenados con nombre cualificado:

Son procedimientos almacenados que se especifican con un nombre completamente cualificado, incluyendo el nombre del esquema, para evitar conflictos de nombres y mejorar la claridad.

### 5. Procedimientos almacenados extendidos:

Son procedimientos que permiten ejecutar funciones en el servidor a través de bibliotecas dinámicas (DLL). Están integrados en el sistema de gestión de bases de datos pero tienen la capacidad de realizar operaciones fuera del ámbito de SQL.

### 6. Procedimientos almacenados de replicación:

Son procedimientos utilizados para gestionar y mantener la replicación de datos entre servidores. Son cruciales para mantener la consistencia de los datos en entornos distribuidos.
